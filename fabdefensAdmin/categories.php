<?php
if($_REQUEST){
	include_once($_SERVER['DOCUMENT_ROOT'].'/connection.php');
	
	define('ADODB_FETCH_DEFAULT',0);
	define('ADODB_FETCH_NUM',1);
	define('ADODB_FETCH_ASSOC',2);
	define('ADODB_FETCH_BOTH',3);
	
	function getFields($obj) {
		$temp  = array();
		$i = 0;
		while (!$obj->EOF) {
			$temp[$i] = $obj->fields;
			$obj->MoveNext();
			$i ++;
		}
		
		return $temp;
    }

	switch($_SERVER['REQUEST_METHOD']) {
		case 'GET' : $data  = &$_GET; 
			break;
		case 'POST': $data  = &$_POST;
			break;
	}
	
	$ADODB_FETCH_MODE = ADODB_FETCH_ASSOC;
	
	if($data["get"] == "list") {
		$rs = $DB->Execute("SELECT * FROM `categories` ORDER BY `name` ASC");
		echo json_encode(getFields($rs));
	}
	
	if($data["get"] == "remove") {
		 $ADODB_FETCH_MODE = ADODB_FETCH_ASSOC;
         
		 $rs = $DB->Execute("DELETE FROM `categories` WHERE id =?", $data["rowId"]);
		 echo json_encode($rs);
	}

}

?>