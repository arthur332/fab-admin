<?php
if($_REQUEST){
	include_once($_SERVER['DOCUMENT_ROOT'].'/connection.php');
	include_once($_SERVER['DOCUMENT_ROOT'].'/fileResize.php');
	
	define('ADODB_FETCH_DEFAULT',0);
	define('ADODB_FETCH_NUM',1);
	define('ADODB_FETCH_ASSOC',2);
	define('ADODB_FETCH_BOTH',3);
	
	
	function getFields($obj) {
		$temp  = array();
		$i = 0;
		while (!$obj->EOF) {
			$temp[$i] = $obj->fields;
			$obj->MoveNext();
			$i ++;
		}
		
		return $temp;
    }

	$data = $_REQUEST;
	
	$rs = $DB->Execute("UPDATE `products` SET `name`=?,`categoryId`=?, `firm`=?,`description`=?,`advantages`=?,`compatibility`=?, `weight`=?, `color1`=?, `color2`=?, `color3`=?, `color4`=?, `width`=?, `height`=?, `length`=?, `whepons`=?, `need`=?, `length2`=? WHERE id =?", array($data["edit-name"],$data["edit-categoryId"], $data["edit-firm"], $data["edit-description"], $data["edit-advantages"], $data["edit-compatibility"], $data["edit-weight"], $data["edit-color1"], $data["edit-color2"], $data["edit-color3"], $data["edit-color4"], $data["edit-width"], $data["edit-height"], $data["edit-length"], $data["edit-whepons"],$data["edit-need"] == "on" ? "1":"0",$data["edit-length2"], $data["edit-id"]));

	if(strlen($data["bindings"]) > 0){
		$bindings = split(",", $data["bindings"]);
		
		foreach ($bindings as $i) {
			$links = $DB ->Execute("INSERT INTO `links` (`weponId`,`productId`) VALUES (?,?)", array($i, $data["edit-id"]));
		}
	}
	
	
	if ($rs === false) {die("failed");}
	
	$fotoId = $data["edit-id"];
	
	$_F =& $_FILES['edit-image'];
		
	if(!empty($_F['error'])){
		 switch($_F['error']){
		  case '1':
			$error = 'Загружаемый файл весит больше чем позволяют настройки сервера (upload_max_filesize directive in php.ini)';
		   break;
		  case '2':
			$error = 'Загружаемый файл весит больше чем позволяют настройки сервера (MAX_FILE_SIZE)';
		   break;
		  case '3':
			$error = 'Загруженный файл был залит частично';
		   break;
		  case '4':
			$error = false;//'Файл не выбран.';
		   break;
		  case '6':
			$error = 'не найдена временная директория';
		   break;
		  case '7':
			$error = 'Не удалось записать файл на диск';
		   break;
		  case '8':
			$error = 'File upload stopped by extension';
		   break;
		  case '999':
		  default:
			$error = 'No error code avaiable';
		 }
		 print_r($error);
	} elseif(empty($_F['tmp_name']) || ($_F['tmp_name'] == 'none')) {
		$error = 'No file was uploaded..';
	} else {
		//echo $_SERVER["SERVER_NAME"];
		if($_SERVER["SERVER_NAME"] == "localhost") {
			img_resize($_F['tmp_name'], "I:/projects/fabdefense/fabdefense/fabdefense/Content/images/products/".$fotoId.".jpg", $width=140, $height=135, $quality=100);
			img_resize($_F['tmp_name'], "I:/projects/fabdefense/fabdefense/fabdefense/Content/images/products/medium/".$fotoId."\/".$fotoId.".jpg", $width=700, $height=0, $quality=100);
			//img_resize($_F['tmp_name'], "I:/projects/fabdefense/fabdefense/fabdefense/Content/images/products/big/".$fotoId."\/".$fotoId.".jpg", $width=700, $height=0, $quality=100);
		}else{
		/*production*/
			img_resize($_F['tmp_name'], "..\Content\images\products\/".$fotoId.".jpg", $width=140, $height=135, $quality=100);
			img_resize($_F['tmp_name'], "..\Content\images\products\medium\/".$fotoId."\/".$fotoId.".jpg", $width=320, $height=0, $quality=100);
			//img_resize($_F['tmp_name'], "..\Content\images\products\big\/".$fotoId."\/".$fotoId.".jpg", $width=700, $height=0, $quality=100);
		
		}
		$error = false;
	}
	echo $error;
	if(!!$rs && !$error){
		header('Location: http://'.$_SERVER['HTTP_HOST'].'/');
	}
}

?>