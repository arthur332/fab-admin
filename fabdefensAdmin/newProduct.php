<?php
if($_REQUEST){
	include_once($_SERVER['DOCUMENT_ROOT'].'/connection.php');
	include_once($_SERVER['DOCUMENT_ROOT'].'/fileResize.php');
	
	define('ADODB_FETCH_DEFAULT',0);
	define('ADODB_FETCH_NUM',1);
	define('ADODB_FETCH_ASSOC',2);
	define('ADODB_FETCH_BOTH',3);
	
	
	function getFields($obj) {
		$temp  = array();
		$i = 0;
		while (!$obj->EOF) {
			$temp[$i] = $obj->fields;
			$obj->MoveNext();
			$i ++;
		}
		
		return $temp;
    }

	$data = $_REQUEST;
	
	$rs = $DB->Execute("INSERT INTO `products` (`name`,`categoryId`, `firm`,`description`,`advantages`, `compatibility`, `weight`, `color1`, `color2`, `color3`, `color4`, `width`, `height`, `length`, `whepons`, `need`,`length2`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", array($data["name"], $data["categoryId"], $data["firm"], $data["description"], $data["advantages"], $data["compatibility"], $data["weight"], $data["color1"], $data["color2"], $data["color3"], $data["color4"], $data["width"], $data["height"], $data["length"], $data["whepons"],$data["need"] == "on" ? "1":"0", $data["length2"]));
	
	if ($rs === false) {die("failed");}
	
	$fotoId = mysql_insert_id();
	
	if(strlen($data["bindings"]) > 0){
		$bindings = split(",", $data["bindings"]);
		
		foreach ($bindings as $i) {
			$links = $DB ->Execute("INSERT INTO `links` (`weponId`,`productId`) VALUES (?,?)", array($i, $fotoId));
		}
	}
	
	/* local
	$galeryDir = "I:/projects/ASKEducation/ASKEducation/Content/images/school/galery/".$fotoId."/";
	$galeryDirBig = "I:/projects/ASKEducation/ASKEducation/Content/images/school/galery/".$fotoId."/big/";*/
	
	if($_SERVER["SERVER_NAME"] == "localhost") {
		$galeryDir = "I:/projects/fabdefense/fabdefense/fabdefense/Content/images/products/medium/".$fotoId."/";
		$galeryDirBig = "I:/projects/fabdefense/fabdefense/fabdefense/Content/images/products/big/".$fotoId."/";
	}else{
		$galeryDir = "../Content/images/products/medium/".$fotoId."/";
		$galeryDirBig = "../Content/images/products/big/".$fotoId."/";
	}
	
	
	if (!is_dir($galeryDir)) {
		mkdir($galeryDir); //create the directory
		chmod($galeryDir, 0777); //make it writable
	}
	
	if (!is_dir($galeryDirBig)) {
		mkdir($galeryDirBig); //create the directory
		chmod($galeryDirBig, 0777); //make it writable
	}
	
	
	$_F =& $_FILES['image'];
		
	if(!empty($_F['error'])){
		 switch($_F['error']){
		  case '1':
			$error = 'Загружаемый файл весит больше чем позволяют настройки сервера (upload_max_filesize directive in php.ini)';
		   break;
		  case '2':
			$error = 'Загружаемый файл весит больше чем позволяют настройки сервера (MAX_FILE_SIZE)';
		   break;
		  case '3':
			$error = 'Загруженный файл был залит частично';
		   break;
		  case '4':
			$error = false;//'Файл не выбран.';
		   break;
		  case '6':
			$error = 'не найдена временная директория';
		   break;
		  case '7':
			$error = 'Не удалось записать файл на диск';
		   break;
		  case '8':
			$error = 'File upload stopped by extension';
		   break;
		  case '999':
		  default:
			$error = 'No error code avaiable';
		 }
		 print_r($error);
	} elseif(empty($_F['tmp_name']) || ($_F['tmp_name'] == 'none')) {
		$error = 'No file was uploaded..';
	} else {
		//echo $_SERVER["SERVER_NAME"];
		if($_SERVER["SERVER_NAME"] == "localhost") {
			img_resize($_F['tmp_name'], "I:/projects/fabdefense/fabdefense/fabdefense/Content/images/products/".$fotoId.".jpg", $width=140, $height=135, $quality=100);
			img_resize($_F['tmp_name'], "I:/projects/fabdefense/fabdefense/fabdefense/Content/images/products/medium/".$fotoId."\/".$fotoId.".jpg", $width=700, $height=0, $quality=100);
			//img_resize($_F['tmp_name'], "I:/projects/fabdefense/fabdefense/fabdefense/Content/images/products/big/".$fotoId."\/".$fotoId.".jpg", $width=700, $height=0, $quality=100);
		}else{
		/*production*/
			img_resize($_F['tmp_name'], "..\Content\images\products\/".$fotoId.".jpg", $width=140, $height=135, $quality=100);
			img_resize($_F['tmp_name'], "..\Content\images\products\medium\/".$fotoId."\/".$fotoId.".jpg", $width=700, $height=0, $quality=100);
			//img_resize($_F['tmp_name'], "..\Content\images\products\big\/".$fotoId."\/".$fotoId.".jpg", $width=700, $height=0, $quality=100);
		
		}
		$error = false;
	}
	
	if(!!$rs && !$error){
		header('Location: http://'.$_SERVER['HTTP_HOST'].'/');
	}else{echo $error;}
	
}

?>