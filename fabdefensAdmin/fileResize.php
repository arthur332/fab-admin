<?php
function img_resize($src, $dest, $width=0, $height=0, $quality=100){
	$rgb=0xffffff;
	if(!file_exists($src)) {
		return 0;
	}
	if(file_exists($dest)) {
		unlink($dest);
	}
	$size = getimagesize($src);

	if($size === false) {
		return 0;
	}
	$format = strtolower(substr($size['mime'], strpos($size['mime'], '/')+1));
	$icfunc = 'imagecreatefrom' . $format;
	if(!function_exists($icfunc)) {
		return 0;
	}
	
	$swh = $size[0]/$size[1];
	$height = $width/$swh;
	$dwh = $width/$height;

	if( $dwh > $swh ){
		$nheight = $height;
		$nwidth = $nheight*$swh;
	}else{
		$nwidth = $width;
		$nheight = $nwidth/$swh;
	}

	$isrc = $icfunc($src);

	if(!$isrc) {
		return 0;
	}
	$idest = imagecreatetruecolor($nwidth, $nheight);

	imagefill($idest, 0, 0, $rgb);
	imagecopyresampled($idest, $isrc, 0, 0, 0, 0, $nwidth, $nheight, $size[0], $size[1]);

	imagejpeg($idest, $dest, $quality);

	imagedestroy($isrc);
	imagedestroy($idest);

	@chmod($dest,0755);

	return true;
}
?>