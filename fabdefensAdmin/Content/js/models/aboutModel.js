﻿/**
 * @name {EditController}
 * @author {Enokyan A/R}
 * @return {Object}
 */
var EditController = function () {
    var self = this;

    this.dataGot = 0;

    this.dataNeed = 1;

    this.getData = function (method) {
        self.method = method;
        askApp.ajaxInterface({ url: "/" + method.concat(".php"), data: { get: "instanse"} }, false).done(self.getDataCallback);
    };

    this.getDataCallback = function (response) {
        self.data = JSON.parse(response)[0];

        self.dataGot ++;
        askApp.dispatchEvent("dataGot");
       
    };

    this.init = function () {
        self.dataGot = 0;
        self.getData();
    };
};

var EC = new EditController();

/**
 * @name {EditModel}
 * @return {undefined}
 */
var EditModel = function () {
    var self = this;
    this.address = ko.observable("");
    this.about = ko.observable("");

    this.save = function () {
        askApp.ajaxInterface({ url: "/" + EC.method.concat(".php"), data: { get: "save",  address: self.address()} }, false);
    }
};

var EM = new EditModel();

$(document).on("dataGot", function() {
    if (EC.dataNeed === EC.dataGot) {
        EM.address(EC.data.address);
        EM.about(EC.data.about);
        ko.applyBindings(EM);
    }
});

