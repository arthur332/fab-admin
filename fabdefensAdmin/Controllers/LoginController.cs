﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace ask.Controllers
{
    public class LoginController : Controller
    {
        private const string User = "admin";
        private const string Pass = "admin";
        //
        // GET: /Login/

        public ActionResult Index()
        {
            return View();

        }

        [HttpPost]
        public ActionResult Login(String username, String password)
        {

            if (username == User && password == Pass)
            {
                FormsAuthentication.SetAuthCookie(username, true);
                return RedirectToAction("Index", "Home");
            }
            return RedirectToAction("Index", "Login");
        }
        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Home");
        }

    }
}
